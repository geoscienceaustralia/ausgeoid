package au.gov.ga.gemd.geoid.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

public final class GeoidUtils {

    private GeoidUtils() {
        super();
    }

    public static String formatMetres(double value) {
        return new DecimalFormat("0.000").format(value);
    }

    public static String formatDegrees(double value) {
        return new DecimalFormat("0.00").format(value);
    }

    public static double getValueAsDouble(String value) {
        double result = Double.parseDouble(value);
        return result;
    }
    
    public static List<Double> parseTxt(InputStream is) throws IOException {

        @SuppressWarnings("unchecked")      
        List<String> lines = IOUtils.readLines(is);
        List<Double> result = null;

        if (lines != null && !lines.isEmpty()) {

            List<String> subLines = skipHeaderInfo(lines);
            result = new ArrayList<Double>(lines.size());

            for (String line : subLines) {
                if (line.length() >= 43) {
                    String valueString = line.substring(43);
                    Double value = getValueAsDouble(valueString);
                    result.add(value);
                }
            }
        }

        return result;

    }
    
    private static List<String> skipHeaderInfo(List<String> lines) {
        return skipLines(3, lines);
    }

    public static List<Double> parseCsv(InputStream is) throws IOException {

        @SuppressWarnings("unchecked")
        List<String> lines = IOUtils.readLines(is);
        List<Double> result = null;

        if (lines != null && !lines.isEmpty()) {
            
            List<String> subLines = skipHeaderInfo(lines);
            
            result = new ArrayList<Double>(lines.size());

            for (String line : subLines) {              
                
                String[] tokens = StringUtils.split(line, ",");
                Double value = getValueAsDouble(tokens[3]);
                result.add(value);
            }
        }

        return result;

    }
    
    private static List<String> skipLines(int numberOfLinesToSkip, List<String> lines) {
        
        // Skip first 3 lines
        List<String> subLines = lines.subList(numberOfLinesToSkip, lines.size());
        return subLines;
    }
    

}
