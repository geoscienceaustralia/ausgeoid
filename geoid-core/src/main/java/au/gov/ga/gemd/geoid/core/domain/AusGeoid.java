package au.gov.ga.gemd.geoid.core.domain;

public enum AusGeoid {

    AusGeoid98("AUSGeoid98", "AUSGeoid98.gsb", "AUSGeoid98"),
    AusGeoid09("AUSGeoid09", "AUSGeoid09.gsb", "AUSGeoid09 V1.01 (11 April 2011)"),
    AusGeoid2020("AUSGeoid2020", "AUSGeoid2020_20170908_sparc.gsb", "AUSGeoid2020_20170908_error_sparc.gsb", "AUSGeoid2020_20170908");

    private String name;
    private String file;
    private String uncertaintyFile;
    private String version;

    private AusGeoid(String name, String file, String version) {
        this.name = name;
        this.file = file;
        this.version = version;
    }

    private AusGeoid(String name, String file, String uncertaintyFile, String version) {
        this.name = name;
        this.file = file;
        this.uncertaintyFile = uncertaintyFile;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public String getFile() {
        return file;
    }

    public String getUncertaintyFile() {
        return uncertaintyFile;
    }

    public String getVersion() {
        return version;
    }
}
