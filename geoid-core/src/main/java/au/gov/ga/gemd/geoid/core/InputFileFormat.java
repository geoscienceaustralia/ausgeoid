package au.gov.ga.gemd.geoid.core;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for inputFileFormatType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="inputFileFormatType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TXT"/>
 *     &lt;enumeration value="CSV"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "inputFileFormatType", namespace = "http://www.ga.gov.au/geodesy/ausgeoid/datatypes")
@XmlEnum
public enum InputFileFormat {

    /**
     * Formatted Text Files (e.g. *.dat, *.prn, *.txt)
     */
    TXT,

    /**
     * Comma Separated Values Files (*.csv)
     */
    CSV;

    public String value() {
        return name();
    }

    public static InputFileFormat fromValue(String v) {
        return valueOf(v);
    }
    
}
