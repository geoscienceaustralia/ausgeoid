package au.gov.ga.gemd.geoid.core.cfg;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.lang.StringUtils;

@SuppressWarnings("serial")
/**
 * Configuration class provides access to all settings
 */
public class GeoidConfiguration implements Serializable {

    private static final String DEFAULT_CONFIGURATION = "geoid.cfg.properties";
    
    private PropertiesConfiguration config;
    
    public static final String GEOID_EXE_PATH_KEY = "geoid.exe.path";
    public static final String GEOID_GRIDFILE_PATH_KEY = "geoid.gridFile.path";
    public static final String GEOID_GRIDFILES_PATH_KEY = "geoid.gridFiles.path";
    public static final String GEOID_GRIDFILE_VERSION_KEY = "geoid.gridFile.version";
    
    public static final String GEOID_EXIT_VALUES_KEY = "geoid.exitValues";
    
    public static final String WATCHDOG_TIMEOUT_KEY = "watchdog.timeout";
    
    public static final long DEFAULT_TIMEOUT = 15000L;
    
    public GeoidConfiguration() throws ConfigurationException {
        this(DEFAULT_CONFIGURATION);
    }
    
    private String geoidExePath;
    private String geoidLibPath;
    private String geoidGridFilePath;
    private String geoidGridFilesPath;
    private String geoidGridFileVersion;
    private int[] geoidExitValues;
    
    public GeoidConfiguration(String fileName) throws ConfigurationException {
        this.config = new PropertiesConfiguration(fileName);
        this.config.setReloadingStrategy(new FileChangedReloadingStrategy());
    }
    
    public String getGeoidExePath() {
        if (this.geoidExePath != null) {
            return this.geoidExePath;
        }

        String value = (String)this.config.getProperty(GEOID_EXE_PATH_KEY);
        return value;
    }
    
    public String getGeoidGridFileVersion() {
        if (this.geoidGridFileVersion != null) {
            return this.geoidGridFileVersion;
        }

        String value = (String)this.config.getProperty(GEOID_GRIDFILE_VERSION_KEY);
        return value;       
    }
    
    public int[] getGeoidExitValues() {
        
        if (this.geoidExitValues != null) {
            return this.geoidExitValues;
        }
    
        @SuppressWarnings("unchecked")
        List<String> value = this.config.getList(GEOID_EXIT_VALUES_KEY);
        
        if (value == null) {
            return null;
        }
        
        int[] result = new int[value.size()];
        
        Iterator<String> iterator = value.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            result[i] = Integer.valueOf(iterator.next());
            i++;
        }
        
        return result;
    }

    public String getGeoidGridFilesPath() {
        if (this.geoidGridFilesPath != null) {
            return this.geoidGridFilesPath;
        }
        return (String) this.config.getProperty(GEOID_GRIDFILES_PATH_KEY);
    }
    
    public String getGeoidGridFilePath() {
        if (this.geoidGridFilePath != null) {
            return this.geoidGridFilePath;
        }
        String value = (String)this.config.getProperty(GEOID_GRIDFILE_PATH_KEY);
        return value;
    }

    public long getWatchdogTimeout() {
        String value = (String) this.config.getProperty(WATCHDOG_TIMEOUT_KEY);
        long timeout = DEFAULT_TIMEOUT;
        if (StringUtils.isNotBlank(value)) {
            timeout = Long.parseLong(value) ;
        }
        
        return timeout;
    }

    
}
