package au.gov.ga.gemd.geoid.core.domain;

public enum InputType {

    DECIMAL_DEGREE ("DD"), DEG_MIN_SEC ("DMS"), ADH_HEIGHT ("AH"), ELLIPSOIDAL_HEIGHT ("EH");
    private String code;

    public String getValue () {
        return (code);
    }
    private InputType (String c) {
        code = c;
    }
}
