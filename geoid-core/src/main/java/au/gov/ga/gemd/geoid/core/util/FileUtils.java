package au.gov.ga.gemd.geoid.core.util;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class FileUtils {

    private static final String USER_DIR = "user.dir";
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);
    
    private static File dir1 = new File(".");
    
    private FileUtils() {
        super();
    }
    
    private static final Object SYNC_OBJECT = new Object();
    
    public static long currentTimeInMillisec() {
        
        long currentTimeMillis = 0L;
        
        synchronized (SYNC_OBJECT) {
            currentTimeMillis = System.currentTimeMillis();
        }
        
        return currentTimeMillis;
    }
    
    public static File createNewFile(String filename) throws IOException {

        File file = new File(filename);
        if (!file.exists()) {
            file.createNewFile();
        }

        return file;

    }
    
    public static void delete(File... files) {
        
        if (files != null) {
        
            for (File file : files) {
                
                if (file != null) {
                
                    boolean isDeleted = file.delete();
                    if (!isDeleted) {
                        LOGGER.warn("The file [" + file.getAbsolutePath() + "] was not deleted.");
                    }               
                }
                
            }
            
        }
    }
    
    public static String getCurrentWorkingDirectory() throws IOException {
        String absolutePath = dir1.getAbsolutePath();
        return absolutePath;
    }
    
    public static String getCurrentDirectory() {
        return System.getProperty(USER_DIR);
    }
    
}
