package au.gov.ga.gemd.geoid.core.domain;

public enum InterpolationOption {

    Latitude("-p"),

    Longitude("-l");

    private String option;

    private InterpolationOption(String option) {
        this.option = option;
    }

    public String getOption() {
        return this.option;
    }
    
}
