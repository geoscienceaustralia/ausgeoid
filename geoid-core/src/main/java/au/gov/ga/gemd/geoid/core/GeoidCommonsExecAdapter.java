package au.gov.ga.gemd.geoid.core;

import static au.gov.ga.gemd.geoid.core.InputFileFormat.CSV;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.gov.ga.gemd.geoid.core.cfg.GeoidConfiguration;
import au.gov.ga.gemd.geoid.core.domain.FileInterpolationOption;
import au.gov.ga.gemd.geoid.core.domain.GeoidValues;
import au.gov.ga.gemd.geoid.core.domain.GeoidValuesParser;
import au.gov.ga.gemd.geoid.core.domain.InputType;
import au.gov.ga.gemd.geoid.core.domain.Interpolant;
import au.gov.ga.gemd.geoid.core.domain.InterpolationOption;
import au.gov.ga.gemd.geoid.core.domain.StandardOption;
import au.gov.ga.gemd.geoid.core.util.GeoidUtils;

/**
 * 
 * @author u42670
 */
@SuppressWarnings("serial")
public class GeoidCommonsExecAdapter implements GeoidAdapter {

    /** Logger for this class */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** Constants define options to run geoid program */
    private static final String NTV2_FILEPATH = "ntv2-filepath";

    private static final String NTV2_FILEPATH_PLACEHOLDER = "${"
            + NTV2_FILEPATH + "}";

    private static final String LATITUDE = "latitude";

    private static final String LATITUDE_PLACEHOLDER = "${" + LATITUDE + "}";

    private static final String LONGITUDE = "longitude";

    private static final String LONGITUDE_PLACEHOLDER = "${" + LONGITUDE + "}";

    private static final String INPUT_FILEPATH = "inputFilepath";

    private static final String INPUT_FILEPATH_PLACEHOLDER = "${"
            + INPUT_FILEPATH + "}";

    private static final String DOT = ".";

    private static final String INPUT_DATA_FILENAME_PREFIX = "input_data";

    private static final Map<String, String> EXEC_ENVIRONMENT = new HashMap<String, String>();

    private GeoidConfiguration config;

    // public static class Parser {

    //     private static final String ERROR = "Error";

    //     public static void parse(String... data) {

    //         for (String datum : data) {

    //             // Any error?
    //             if (StringUtils.containsIgnoreCase(datum, ERROR)) {

    //                 String message = StringUtils.substringAfter(datum, ERROR);

    //                 throw new GeoidInterpolationException(ERROR + message);

    //             }

    //         }

    //     }

    // }

    /**
     * Default constructor
     */
    public GeoidCommonsExecAdapter() {
        try {
            this.config = new GeoidConfiguration();

        } catch (ConfigurationException e) {
            throw new GeoidServiceException("Cannot load configuration", e);
        }
    }

    /**
     * Constructor with the configuration
     * 
     * @param config
     */
    public GeoidCommonsExecAdapter(GeoidConfiguration config) {
        this.config = config;
    }

    /**
     * @see {@link GeoidAdapter#interpolate(Interpolant)}
     */
    public GeoidValues interpolate(
        Interpolant     interpolant)
    {
        double          latitude    = interpolant.getLatitude();
        double          longitude   = interpolant.getLongitude();

        CommandLine     commandLine = CommandLine.parse(this.config.getGeoidExePath());
        
        if (InputType.DECIMAL_DEGREE.getValue().equals(interpolant.getDegreeType())) {
            commandLine.addArgument(StandardOption.DecimalDegree.getOption());
        }
        commandLine.addArgument(StandardOption.CoordinatesInterpolation.getOption());
        commandLine.addArgument(StandardOption.NTv2GridFilePath.getOption());
        commandLine.addArgument(NTV2_FILEPATH_PLACEHOLDER);
        commandLine.addArgument(InterpolationOption.Latitude.getOption());
        
    
        commandLine.addArgument(LATITUDE_PLACEHOLDER);
        commandLine.addArgument(InterpolationOption.Longitude.getOption());
        commandLine.addArgument(LONGITUDE_PLACEHOLDER);

        HashMap<String, String> substitutionMap = new HashMap<String, String>();
        substitutionMap.put(NTV2_FILEPATH, absoluteGeoidFileName(interpolant.getAusGeoid().getFile()));
        substitutionMap.put(LATITUDE, Double.toString(latitude));
        substitutionMap.put(LONGITUDE, Double.toString(longitude));

        commandLine.setSubstitutionMap(substitutionMap);

        this.logger.debug("interpolate : commandLine : " + commandLine.toString());

        DefaultExecutor executor = buildExecutor();

        setWatchDog(executor);

        String output = executeCommand(commandLine, executor);

        GeoidValues geoidValues = GeoidValuesParser.extractValues(output);

        double height = interpolant.getHeight();
        double nValue = geoidValues.getNValue();
        double orthometricHeight, ellipsoidalHeight;
        if (interpolant.getHeightType ().equals (InputType.ELLIPSOIDAL_HEIGHT.getValue()) ) {
            orthometricHeight = height - nValue;
            ellipsoidalHeight = height;
        } else {
            ellipsoidalHeight = height + nValue;
            orthometricHeight = height;
            
        }
        
        geoidValues.setOrthometricHeight(orthometricHeight);
        geoidValues.setEllipsoidalHeight(ellipsoidalHeight);

        if (interpolant.getAusGeoid().getUncertaintyFile() != null) {
            substitutionMap.put(NTV2_FILEPATH, absoluteGeoidFileName(interpolant.getAusGeoid().getUncertaintyFile()));
            commandLine.setSubstitutionMap(substitutionMap);
            DefaultExecutor uncertaintyExecutor = buildExecutor();
            setWatchDog(uncertaintyExecutor);
            String uncertaintyOutput = executeCommand(commandLine, uncertaintyExecutor);
            geoidValues.setUncertainty(new Double(GeoidValuesParser.extractValues(uncertaintyOutput).getNValue()));
        }
        return geoidValues;
    }

    private String absoluteGeoidFileName(String geoidFile) {
        return this.config.getGeoidGridFilesPath() + geoidFile;
    }

    /**
     * Build default executor to run the command line
     * 
     * @return default executor
     */
    private DefaultExecutor buildExecutor() {
        DefaultExecutor executor = new DefaultExecutor();
        int[] exitValues = this.config.getGeoidExitValues();
        executor.setExitValues(exitValues);
        return executor;
    }

    /**
     * @see {@link GeoidAdapter#interpolate(InputStream, InputFileFormat)}
     */
    public InputStream interpolate(
            Interpolant     interpolant,
            InputStream     input,
            InputFileFormat inputFileFormat) {
        assert input != null : "Inputstream cannot be null";

        CommandLine commandLine = CommandLine.parse(this.config.getGeoidExePath());
        commandLine.addArgument(StandardOption.CoordinatesFileInterpolation.getOption());
        
        if (interpolant.getDegreeType ().equals (InputType.DECIMAL_DEGREE.getValue()) )
            commandLine.addArgument(StandardOption.DecimalDegree.getOption());
        
        // These option are linked and should stay together they add -r 0 or -r 1
        commandLine.addArgument(FileInterpolationOption.HeightDirectionOption.getOption());
        if (interpolant.getHeightType ().equals (InputType.ADH_HEIGHT.getValue()) ) {
                commandLine.addArgument(FileInterpolationOption.ReverseFlag.getOption());
        } else {
                commandLine.addArgument(FileInterpolationOption.ForwardFlag.getOption());
        }
        //end linked option
        
        
        commandLine.addArgument(StandardOption.NTv2GridFilePath.getOption());
        commandLine.addArgument(NTV2_FILEPATH_PLACEHOLDER);
        commandLine.addArgument(FileInterpolationOption.InputFilePath
                .getOption());
        commandLine.addArgument(INPUT_FILEPATH_PLACEHOLDER);

        String inputFilePath = null;
        String tempInputFilename = null;
        File tempInputFile = null;

        InputStream result = null;

        long timestamp = au.gov.ga.gemd.geoid.core.util.FileUtils
                .currentTimeInMillisec();
        tempInputFilename = INPUT_DATA_FILENAME_PREFIX + timestamp;

        String uncertaintyInputFilePath = null;

        try {
            // JRF TODO Externalize /tmp
            String tempInputFileName = "/tmp/" + tempInputFilename + DOT
                    + inputFileFormat;

            tempInputFile = new File(tempInputFileName);        
            if (tempInputFile.createNewFile() == false) {
                if (tempInputFile.delete() == false) {
                    throw new GeoidInterpolationException(
                            "Cannot create temp input file");
                }
            }

            String content = IOUtils.toString(input);
            if (CSV.equals(inputFileFormat)) {
                GeoidAdapterHelper.validateInputContent(content);
            }

            FileUtils.writeStringToFile(tempInputFile, content);

            inputFilePath = tempInputFile.getCanonicalPath();

            HashMap<String, String> substitutionMap = new HashMap<String, String>();
            substitutionMap.put(NTV2_FILEPATH, this.config.getGeoidGridFilesPath() + interpolant.getAusGeoid().getFile());
            substitutionMap.put(INPUT_FILEPATH, inputFilePath);

            commandLine.setSubstitutionMap(substitutionMap);
            
            DefaultExecutor executor = buildExecutor();

            setWatchDog(executor);

            String filePath = StringUtils.substringBeforeLast(inputFilePath,
                    File.separator);

            executeCommand(commandLine, executor);

            String tempUncertaintyOutputFilename = null;
            if (interpolant.getAusGeoid().getUncertaintyFile() != null) {
                String extension = FilenameUtils.getExtension(inputFilePath);
                uncertaintyInputFilePath = FilenameUtils.removeExtension(inputFilePath) + "_uncertainty." + extension;

                FileUtils.copyFile(new File(inputFilePath), new File(uncertaintyInputFilePath));

                substitutionMap.put(NTV2_FILEPATH, absoluteGeoidFileName(interpolant.getAusGeoid().getUncertaintyFile()));
                substitutionMap.put(INPUT_FILEPATH, uncertaintyInputFilePath);
                commandLine.setSubstitutionMap(substitutionMap);

                DefaultExecutor uncertaintyExecutor = buildExecutor();
                setWatchDog(uncertaintyExecutor);

                executeCommand(commandLine, uncertaintyExecutor);

                tempUncertaintyOutputFilename = tempInputFilename + "_uncertainty_out." + inputFileFormat;
            }

            // Produce the output
            String tempOutputFilename = tempInputFilename + "_out." + inputFileFormat;

            if (this.logger.isDebugEnabled()) {
                this.logger.debug("Temp Input File: " + tempInputFileName);
                this.logger.debug("Temp Output File: " + tempOutputFilename);
                }
            String output = generateOutput(interpolant, filePath, tempOutputFilename, tempUncertaintyOutputFilename);

            result = new ByteArrayInputStream(output.getBytes());
        } catch (IOException e) {
            throw new GeoidInterpolationException(e.getMessage(), e);
        } finally {
            au.gov.ga.gemd.geoid.core.util.FileUtils.delete(tempInputFile);
            if (uncertaintyInputFilePath != null) {
                au.gov.ga.gemd.geoid.core.util.FileUtils.delete(new File(uncertaintyInputFilePath));
            }
        }

        return result;
    }

    /**
     * Generate output content based on the given file
     * 
     * @param outputFilePath
     * @param tempOutputFilename
     * @return output content in byte array
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    private String generateOutput(
            Interpolant interpolant,
            String outputFilePath,
            String tempOutputFilename,
            String tempUncertaintyOutputFilename) throws IOException {

        File tempOutputFile = null;
        File tempUncertaintyOutputFile = null;
        StringBuffer contentBuffer = new StringBuffer();

        try {

            // Read the output file after success transformation
            tempOutputFile = new File(outputFilePath + File.separator + tempOutputFilename);

            if (!tempOutputFile.exists()) {
                throw new IOException("Cannot find output file");
            }

            if (tempOutputFile.length() == 0) {
                throw new GeoidInterpolationException("No output file was generated");
            }

            List<String> outputLines = (List<String>) FileUtils.readLines(tempOutputFile);

            List<String> uncertaintyLines = null;

            if (tempUncertaintyOutputFilename != null) {
                tempUncertaintyOutputFile = new File(outputFilePath + File.separator + tempUncertaintyOutputFilename);
                uncertaintyLines = (List<String>) FileUtils.readLines(tempUncertaintyOutputFile);
            }

            Date dateProcessed = Calendar.getInstance().getTime();

            String headerInfo = GeoidAdapterHelper.substituteHeaderInfo(
                    dateProcessed, interpolant.getAusGeoid().getVersion());

            contentBuffer.append(headerInfo);
            contentBuffer.append("Point id,Latitude,Longitude,Height (input),AHD height,Ellipsoidal height,AUSGeoid value,AUSGeoid uncertainty,DoV PM,DoV PV\r\n");
            for (int i = 2; i < outputLines.size(); i++) {
                OutputLine parsedLine = new OutputLine(outputLines.get(i));
                StringBuffer line = new StringBuffer();
                line.append(parsedLine.point);
                line.append(",");
                line.append(parsedLine.latitude);
                line.append(",");
                line.append(parsedLine.longitude);
                line.append(",");
                line.append(parsedLine.origHt);
                line.append(",");
                if (InputType.ADH_HEIGHT.getValue().equals(interpolant.getHeightType())) {
                    line.append(GeoidUtils.formatMetres(parsedLine.origHt));
                    line.append(",");
                    line.append(GeoidUtils.formatMetres(parsedLine.computedHt));
                } else if (InputType.ELLIPSOIDAL_HEIGHT.getValue().equals(interpolant.getHeightType())) {
                    line.append(GeoidUtils.formatMetres(parsedLine.computedHt));
                    line.append(",");
                    line.append(GeoidUtils.formatMetres(parsedLine.origHt));
                } else {
                    line.append("error: unknown input height");
                }
                line.append(",");
                line.append(GeoidUtils.formatMetres(parsedLine.nValue));
                line.append(",");
                if (uncertaintyLines != null) {
                    line.append(GeoidUtils.formatMetres(new OutputLine(uncertaintyLines.get(i)).nValue));
                }
                line.append(",");
                line.append(GeoidUtils.formatDegrees(parsedLine.dMerid));
                line.append(",");
                line.append(GeoidUtils.formatDegrees(parsedLine.dPrimV));
                contentBuffer.append(line.toString());
                contentBuffer.append("\r\n");
            }
        } finally {
            au.gov.ga.gemd.geoid.core.util.FileUtils.delete(tempOutputFile);
            if (tempUncertaintyOutputFile != null) {
                au.gov.ga.gemd.geoid.core.util.FileUtils.delete(tempUncertaintyOutputFile);
            }
        }
        return contentBuffer.toString();
    }

    private class OutputLine {
        final public int point;
        final public double latitude;
        final public double longitude;
        final public double origHt;
        final public double computedHt;
        final public double nValue;
        final public double dMerid;
        final public double dPrimV;

        public OutputLine(String line) {
            StringTokenizer tokens = new StringTokenizer(line, ",");

            this.point = Integer.valueOf(tokens.nextToken().trim());
            this.latitude = Double.valueOf(tokens.nextToken().trim());
            this.longitude = Double.valueOf(tokens.nextToken().trim());
            this.origHt = Double.valueOf(tokens.nextToken().trim());
            this.computedHt = Double.valueOf(tokens.nextToken().trim());
            this.nValue = Double.valueOf(tokens.nextToken().trim());
            this.dMerid = Double.valueOf(tokens.nextToken().trim());
            this.dPrimV = Double.valueOf(tokens.nextToken().trim());
        }
    }

    /**
     * Get environment path to run the program
     * 
     * @return
     */
    // New binary uses RPATH during make - LD_LIBRARY_PATH becomes
    // reduntant
    private Map<String, String>
    getExecEnvironmentPath()
    {
        // JRF TODO remove this method
        //if (GeoidAdapterHelper.isWindows()) {

            //return null;

        //} else {

            
            //if (!EXEC_ENVIRONMENT.containsKey(LD_LIBRARY_PATH)) {

                //String ldLibraryPath = this.config.getGeoidLibPath()
                        //+ REQUIRED_UNIX_LIB;

                //EXEC_ENVIRONMENT.put(LD_LIBRARY_PATH, ldLibraryPath);
            //}

        //}

        //return EXEC_ENVIRONMENT;
        return (null);
    }

    /**
     * Run the given command line
     * 
     * @param commandLine
     * @param executor
     * @return result
     */
    private String executeCommand(CommandLine commandLine,
            DefaultExecutor executor) {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayOutputStream err = new ByteArrayOutputStream();

        PumpStreamHandler pumpStreamHandler = new PumpStreamHandler(out, err);
        executor.setStreamHandler(pumpStreamHandler);

        if (this.logger.isDebugEnabled()) {
            this.logger.debug("Command Line: " + commandLine.toString());
        }

        try {

            Map<String, String> execEnvironmentPath = getExecEnvironmentPath();
            int resultCode = executor.execute(commandLine, execEnvironmentPath);
            String output = out.toString();
            String error = err.toString();

            if (this.logger.isDebugEnabled()) {
                this.logger.debug("Output: " + output);
                this.logger.debug("Error: " + error);
            }
            if (resultCode != 0) {
                throw new GeoidInterpolationException("Interpolation failed, see log file more information.");
            }
            return output;
        } catch (IOException e) {
            throw new GeoidSystemException(e.getMessage(), e);
        }
    }

    private void setWatchDog(DefaultExecutor executor) {
        long timeout = this.config.getWatchdogTimeout();
        ExecuteWatchdog watchdog = new ExecuteWatchdog(timeout);
        executor.setWatchdog(watchdog);
    }

    /**
     * @see {@link GeoidAdapter#interpolate(InputStream)}
     */
    //public InputStream interpolate(InputStream input) {
        //return interpolate(input, CSV);
    //}
    
    public InputStream
    interpolate(
            Interpolant     interpolant,
            InputStream     input) {
        return interpolate(interpolant, input, CSV);
    }
}
