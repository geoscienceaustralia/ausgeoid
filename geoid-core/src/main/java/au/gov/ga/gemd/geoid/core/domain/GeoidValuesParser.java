package au.gov.ga.gemd.geoid.core.domain;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.gov.ga.gemd.geoid.core.GeoidInterpolationException;

public class GeoidValuesParser {

    private static final String EQUAL = "=";

    /** Logger of this class */
    private static Logger logger = LoggerFactory.getLogger(GeoidValuesParser.class.getName());

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    /**
     * Represents the position of the line within the output
     * returned by command-line program
     *
     */
    public interface LinePosition {

        public static final int ERROR = 18;

        public static final int N_VALUE = 19;

        public static final int DEFL_PRIME_M = 20;

        public static final int DEFL_PRIME_L = 21;

    }

    public static void handleError(List<String> lines) {

        if (lines.size() - 1  < LinePosition.ERROR) {
            return;
        }

        String forthLine = lines.get(LinePosition.ERROR);

        if (!StringUtils.contains(forthLine, ERROR)) {
            return;
        }

        ListIterator<String> listIterator = lines.listIterator(LinePosition.ERROR);
        StringBuilder sb = new StringBuilder();

        while(listIterator.hasNext()) {
            String next = listIterator.next();
            sb.append(next);
        }

        throw new GeoidInterpolationException(sb.toString());
    }

    private static final String ERROR = "Error";

    /**
     * Parse the output (String) based on the line number
     *
     * Line 6 - N Value
     * Line 7 - DEFL Prime M
     * Line 8 - DEFL Prime V
     *
     * @param is
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<String> parseResult(InputStream is) {

        List<String> lines = null;

        try {


            lines = IOUtils.readLines(is);

        } catch (IOException e) {
            throw new GeoidInterpolationException("Cannot get result");
        }

        if (lines == null || lines.isEmpty() || lines.size() < LinePosition.ERROR) {
            throw new GeoidInterpolationException("Cannot get result");
        }

        if (logger.isDebugEnabled()) {
            logger.debug(lines.toString());
        }

        handleError(lines);

        return lines;
    }

    public static GeoidValues extractValues(String data) {

        String[] tokens = StringUtils.splitByWholeSeparatorPreserveAllTokens(data, LINE_SEPARATOR);

        List<String> lines = Arrays.asList(tokens);

        return extractValues(lines);

    }

    public static GeoidValues extractValues(List<String> lines) {


        for(int i = 0; i < lines.size(); i++){
            logger.debug("Line : " + i + ":    " + lines.get(i));
        }

        double nValue = 0.0;
        double deflPrimeM = 0.0;
        double deflPrimeV = 0.0;

        String temp = lines.get(LinePosition.N_VALUE);
        logger.debug("N_VALUE : " + temp);
        nValue = getValueAsDouble(temp);
        logger.debug("nValue : " + nValue);

        temp = lines.get(LinePosition.DEFL_PRIME_M);
        logger.debug("DEFL_PRIME_M : " + temp);
        deflPrimeM = getValueAsDouble(temp);
        logger.debug("deflPrimeM : " + deflPrimeM);

        temp = lines.get(LinePosition.DEFL_PRIME_L);
        logger.debug("DEFL_PRIME_L : " + temp);
        deflPrimeV = getValueAsDouble(temp);
        logger.debug("deflPrimeV : " + deflPrimeV);

        GeoidValues geoidValues = GeoidValues.createResult(nValue,deflPrimeM, deflPrimeV);

        return geoidValues;

    }

    private static double getValueAsDouble(String value) {
        String str = StringUtils.substringAfterLast(value, EQUAL).trim();
        double result = StringUtils.isNotEmpty(str) ? Double.parseDouble(str) : 0.0;
        return result;
    }

    public interface Constant {

        public static final String N_VALUE = "N VALUE";

        public static final String DEFL_PRIME_M = "DEFL PRIME M";

        public static final String DEFL_PRIME_L = "DEFL PRIME L";

    }

}
