package au.gov.ga.gemd.geoid.core;

public class GeoidServiceException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -2323393692207617058L;

    public GeoidServiceException(String message) {
        super(message);
    }

    public GeoidServiceException(Throwable cause) {
        super(cause);
    }

    public GeoidServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
