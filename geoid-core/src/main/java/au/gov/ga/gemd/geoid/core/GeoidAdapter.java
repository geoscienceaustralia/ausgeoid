package au.gov.ga.gemd.geoid.core;

import java.io.InputStream;
import java.io.Serializable;

import au.gov.ga.gemd.geoid.core.domain.GeoidValues;
import au.gov.ga.gemd.geoid.core.domain.Interpolant;

public interface GeoidAdapter extends Serializable {
    
    /**
     * Interpolate coordinates
     * 
     * @param interpolant
     * @return
     */
    public GeoidValues interpolate(Interpolant interpolant);
    
    /**
     * Interpolate a collection of coordinates defined in a file
     * 
     * @param input file stream
     * @param {@link {@link InputFileFormat}
     * @return a stream of collection of interpolated result
     */
    public InputStream interpolate(Interpolant interpolant, InputStream input, InputFileFormat inputFileFormat);
    
    /**
     * Interpolate a collection of coordinates defined in a file with csv format as default
     * 
     * @param input file stream
     * @return a stream of collection of interpolated result
     */
    public InputStream interpolate(Interpolant interpolant, InputStream input);
    
}
