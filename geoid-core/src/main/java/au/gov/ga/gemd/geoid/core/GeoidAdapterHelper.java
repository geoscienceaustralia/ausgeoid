package au.gov.ga.gemd.geoid.core;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;

/**
 * The helper class provides utility functionality to the main adapter
 * @author u42670
 *
 */
public final class GeoidAdapterHelper {
    
    /** Constant defines the os name property in path */
    private static final String OS_NAME = "os.name";
    
    /** Constant defines the delimiter of the input file. This is only valid with CSV format */
    private static final String CONTENT_DELIMiTER = ",";
    
    /** Constant defines the template of the header of the output file */
    private static final String OUTPUT_HEADER_TEMPLATE;

    /** Constant defines the place holders within the header of the output file */
    private static final String[] HEADER_PLACE_HOLDER = { 
        "${dateProcessed}",
        "${timeProcessed}", 
        "${version}" 
    };

    /** Constant defines the formatter of the processed date field within the header of the output file */
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(
            "dd-MM-yyyy");
    
    /** Constant defines the formatter of the processed time field within the header of the output file */
    private static final SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat(
            "h:mm a");
    
    /* 
     * Initialise the header template of the output file
     */
    static {
        
        String lineSeparator = System.getProperty("line.separator");

        StringBuilder sb = new StringBuilder();
        sb.append("Date Processed: ").append("${dateProcessed}")
                .append(lineSeparator);
        sb.append("Time Processed: ").append("${timeProcessed}")
                .append(lineSeparator);
        sb.append("Grid File Version: ").append("${version}")
                .append(lineSeparator);
        OUTPUT_HEADER_TEMPLATE = sb.toString();
        
    }

    /**
     * Private default constructor to prevent the class being instantiated externally 
     */
    private GeoidAdapterHelper() {
        super();
    }
    
    /**
     * Checks what the platform is
     * @return true if it is Windows, otherwise false
     */
    public static boolean isWindows() {

        String os = System.getProperty(OS_NAME).toLowerCase();
        // windows
        return (os.indexOf("win") >= 0);

    }
    
    /**
     * Validate the content of input file
     * @param content
     */
    public static void validateInputContent(String content) {

        if (StringUtils.isBlank(content)) {
            return;
        }

        StringTokenizer tokenizer = new StringTokenizer(content, CONTENT_DELIMiTER);

        int countTokens = tokenizer.countTokens();
        if (countTokens <= 1) {
            throw new ValidationException(
                    "The input file cannot be processed because it is not csv format");
        }

    }
    
    /**
     * Substitutes header info with given date processed and grid file version
     * @param dateProcessed
     * @param gridFileVersion
     * @return header info with date processed and grid file version
     */
    public static String substituteHeaderInfo(Date dateProcessed, String gridFileVersion) {

        String dateFormat = DATE_FORMATTER.format(dateProcessed);
        String timeFormat = TIME_FORMATTER.format(dateProcessed);

        if (StringUtils.isEmpty(gridFileVersion)) {
            gridFileVersion = "UNKNOWN";
        } 

        String[] substituteValues = { dateFormat, timeFormat, gridFileVersion };

        String result = StringUtils.replaceEach(OUTPUT_HEADER_TEMPLATE,
                HEADER_PLACE_HOLDER, substituteValues);

        return result;

    }
    
}
