package au.gov.ga.gemd.geoid.core;

@SuppressWarnings("serial")
public class GeoidSystemException extends RuntimeException {

    public GeoidSystemException(String message) {
        super(message);
    }

    public GeoidSystemException(Throwable cause) {
        super(cause);
    }

    public GeoidSystemException(String message, Throwable cause) {
        super(message, cause);
    }

}
