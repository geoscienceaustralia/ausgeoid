package au.gov.ga.gemd.geoid.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class GeoidInterpolationException extends RuntimeException {
        /** Logger for this class */
    private Logger logger = LoggerFactory.getLogger(getClass());

    public GeoidInterpolationException() {
        super();
    }
    
    /**
     * 
     * @param message
     * @param faultInfo
     */
    public GeoidInterpolationException(String message) {
        super(message);
        logger.debug(message);
    }
    
    /**
     * 
     * @param message
     * @param faultInfo
     */
    public GeoidInterpolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
