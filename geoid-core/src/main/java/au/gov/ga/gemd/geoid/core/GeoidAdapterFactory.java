package au.gov.ga.gemd.geoid.core;

import org.apache.commons.configuration.ConfigurationException;

import au.gov.ga.gemd.geoid.core.cfg.GeoidConfiguration;

public class GeoidAdapterFactory {

    private static GeoidAdapter geoidAdapterInstance;

    private GeoidAdapterFactory() {
        super();
    }

    public synchronized static GeoidAdapter getInstance() {

        if (geoidAdapterInstance == null) {
            GeoidConfiguration config = null;

            try {
                config = new GeoidConfiguration();

            } catch (ConfigurationException e) {
                throw new GeoidServiceException("Cannot load configuration", e);
            }

            geoidAdapterInstance = new GeoidCommonsExecAdapter(config);
        }

        return geoidAdapterInstance;
    }

}
