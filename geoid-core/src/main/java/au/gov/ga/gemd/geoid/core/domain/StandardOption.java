package au.gov.ga.gemd.geoid.core.domain;

public enum StandardOption {
    
    CoordinatesInterpolation("-i"),

    CoordinatesFileInterpolation("-f"),
    
    NTv2GridFilePath("-n"),

    DecimalDegree("--decimal-degrees");
    



    private String option;

    private StandardOption(String option) {
        this.option = option;
    }

    public String getOption() {
        return this.option;
    }
}
