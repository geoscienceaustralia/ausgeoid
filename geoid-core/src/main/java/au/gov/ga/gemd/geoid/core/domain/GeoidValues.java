package au.gov.ga.gemd.geoid.core.domain;

import java.io.Serializable;
import java.text.DecimalFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.gov.ga.gemd.geoid.core.util.GeoidUtils;

/**
 * Holds the value of the outcome being executed by the
 * command-line program
 *
 * @author u42670
 *
 */
@SuppressWarnings("serial")
public class GeoidValues implements Serializable {

    private double nValue;

    private Double uncertainty;

    private double orthometricHeight;
    private double ellipsoidalHeight;

    private double deflectionPrimeMeridian;

    private double deflectionPrimeVertical;
    
    public static GeoidValues createResult(double nValue, double deflPrimeMedian, double deflPrimeVertical) {

        GeoidValues result = new GeoidValues();

        result.setNValue(nValue);

        result.setDeflectionPrimeMeridian(deflPrimeMedian);

        result.setDeflectionPrimeVertical(deflPrimeVertical);

        return result;
    }

    /**
     * Default constructor
     */
    public GeoidValues() {
        super();
    }

    public double getNValue() {
        return this.nValue;
    }

    public void setNValue(double nValue) {
        this.nValue = nValue;
    }

    public double getDeflectionPrimeMeridian() {
        return this.deflectionPrimeMeridian;
    }

    public void setDeflectionPrimeMeridian(double deflectionPrimeMeridian) {
        this.deflectionPrimeMeridian = deflectionPrimeMeridian;
    }

    public double getDeflectionPrimeVertical() {
        return this.deflectionPrimeVertical;
    }

    public void setDeflectionPrimeVertical(double deflectionPrimeVertical) {
        this.deflectionPrimeVertical = deflectionPrimeVertical;
    }

    public String getUncertaintyString() {
        if (this.uncertainty == null) {
            return null;
        } else {
            return GeoidUtils.formatMetres(this.uncertainty);
        }
    }

    public void setUncertainty(Double uncertainty) {
        this.uncertainty = uncertainty;
    }

    public double getOrthometricHeight() {
        return this.orthometricHeight;
    }

    public void setOrthometricHeight(double orthometricHeight) {
        this.orthometricHeight = orthometricHeight;
    }

    public String getOrthometricHeightString() {
        return GeoidUtils.formatMetres(this.orthometricHeight);
    }
    
    public double getEllipsoidalHeight() {
        return ellipsoidalHeight;
    }

    public void setEllipsoidalHeight(double ahdHeight) {
        this.ellipsoidalHeight = ahdHeight;
    }
    
    public String getEllipsoidalHeightString() {
        return GeoidUtils.formatMetres(this.ellipsoidalHeight);
    }
    
    public String getNValueString(){
        return GeoidUtils.formatMetres(this.nValue);
    }

    public String getDeflectionPrimeMeridianString(){
        return  GeoidUtils.formatDegrees(this.deflectionPrimeMeridian);
    }

    public String getDeflectionPrimeVerticalString(){
        return GeoidUtils.formatDegrees(this.deflectionPrimeVertical);
    }
}
