package au.gov.ga.gemd.geoid.core.domain;

/**
 * Represents interpolation option that can be passed to
 * the command-line program
 * 
 * @author u42670
 *
 */
public enum FileInterpolationOption {

    InputFilePath("-t"),

    HeightDirectionOption("-r"),

    ReverseFlag("0"), // default option for geoid
    ForwardFlag("1");
    
    private String option;

    private FileInterpolationOption(String option) {
        this.option = option;
    }

    public String getOption() {
        return this.option;
    }
    
}
