package au.gov.ga.gemd.geoid.core;

import static au.gov.ga.gemd.geoid.core.util.GeoidUtils.parseCsv;
import static au.gov.ga.gemd.geoid.core.util.GeoidUtils.parseTxt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import au.gov.ga.gemd.geoid.core.domain.GeoidValues;
import au.gov.ga.gemd.geoid.core.domain.Interpolant;

public class TestGeoidCommonsExecAdapter {
    
    private GeoidAdapter wrapper;
    
    private static final String TEST_CSV_DATA = "test_data.csv";
    private static final String TEST_TXT_DATA = "test_data.dat";
    
    @Before
    public void setUp() {
        this.wrapper = new GeoidCommonsExecAdapter();
    }
    
    @Test
    public void testInterpolatePoint1() {
        
        Interpolant interpolant = new Interpolant(-10.584057510, 142.210992800);
    
        GeoidValues geoidValues = this.wrapper.interpolate(interpolant);
        
        assertNotNull(geoidValues);
        
        final double EXPECTED_N_VALUE = 71.4165;
                
        assertEquals(EXPECTED_N_VALUE, geoidValues.getNValue(), 0.0);
        assertEquals(-4.00497, geoidValues.getDeflectionPrimeMeridian(), 0.0);
        assertEquals(-5.52097, geoidValues.getDeflectionPrimeVertical(), 0.0);
    }
    
    @Test
    public void testInterpolatePoint2() {
        
        double height = 52;
        
        Interpolant interpolant = new Interpolant(-38.93875308, 145.53129736);
        interpolant.setHeight(height);
        
    
        GeoidValues geoidValues = this.wrapper.interpolate(interpolant);
        
        assertNotNull(geoidValues);
        
        final double EXPECTED_N_VALUE = 2.30953;
        
        assertEquals(EXPECTED_N_VALUE, geoidValues.getNValue(), 0.0);
        // JRF Quick fix for web migration
        //assertEquals(ellipsodialHeight - EXPECTED_N_VALUE, geoidValues.getOrthometricHeight() , 0.0);
        assertEquals(-5.3656, geoidValues.getDeflectionPrimeMeridian(), 0.0);
        assertEquals(-3.53603, geoidValues.getDeflectionPrimeVertical(), 0.0);
    }
    
    @Test
    public void testInterpolateTxtFile() throws Exception {
        
        InputStream is = ClassLoader.getSystemResourceAsStream(TEST_TXT_DATA);
        assertNotNull(is);
        /*
         * JRF - quick fix for web migration    
        InputStream output = this.wrapper.interpolate(is, InputFileFormat.TXT);
        assertNotNull(output);

        List<Double> tokens = parseTxt(output);

        assertEquals(-41.791, tokens.get(0), 0.0);
        assertEquals(-37.364, tokens.get(1), 0.0);
        assertEquals(-36.188, tokens.get(2), 0.0);
        assertEquals(-38.095, tokens.get(3), 0.0);
        assertEquals(-37.682, tokens.get(4), 0.0);
        assertEquals(-36.856, tokens.get(5), 0.0);
        assertEquals(-39.042, tokens.get(6), 0.0);
        assertEquals(-38.516, tokens.get(7), 0.0);
        assertEquals(-40.449, tokens.get(8), 0.0);
        assertEquals(-40.403, tokens.get(9), 0.0);
    */
    }

    @Test
    public void testInterpolateCsvFile() throws Exception {
        
        InputStream is = ClassLoader.getSystemResourceAsStream(TEST_CSV_DATA);
        assertNotNull(is);
        /*
         * JRF - quick fix for web migration    
        InputStream output = this.wrapper.interpolate(is, InputFileFormat.CSV);
        assertNotNull(output);

        List<Double> tokens = parseCsv(output);

        assertEquals(58.744, tokens.get(0), 0.0);
        assertEquals(2.821, tokens.get(1), 0.0);
        assertEquals(12.764, tokens.get(2), 0.0);
        assertEquals(28.538, tokens.get(3), 0.0);
        assertEquals(28.545, tokens.get(4), 0.0);
        assertEquals(118.765, tokens.get(5), 0.0);
        assertEquals(78.209, tokens.get(6), 0.0);
        assertEquals(6.752, tokens.get(7), 0.0);
        assertEquals(13.392, tokens.get(8), 0.0);
        assertEquals(12.204, tokens.get(9), 0.0);
    */
    }
    
    
    @Test
    public void testFailedInterpolate() {
        
        Interpolant interpolant = new Interpolant(38.93875308, 145.53129736);
    
        try {
            this.wrapper.interpolate(interpolant);
            fail("Should have thrown exception");
        } catch(GeoidInterpolationException ex) {
            
        }
        
    }
    
}
