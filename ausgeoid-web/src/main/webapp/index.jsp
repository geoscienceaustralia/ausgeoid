<!DOCTYPE html>
<html class="no-js" lang="en-us">
<head>
<title>AUSGeoid - Geoscience Australia</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- DC //-->
<meta name="DC.Description" content="Geoscience Australia is Australia's pre-eminent public sector geoscience organisation. We are the nation's trusted advisor on the geology and geography of Australia. We apply science and technology to describe and understand the Earth for the benefit of Australia." />
<link rel="shortcut icon" type="image/ico" href="images/favicon.png" />

<!-- Styles -->
<script src="js/modernizr.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css" media="screen" />
<link rel="stylesheet" href="css/layout.css" media="all and (min-width: 47.5em)">

<!-- Scripts - Remove as required //--> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script> 
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script> 
<script src="js/bootstrap.min.js"></script> <!-- bootstrap --> 
<script src="js/footablejs.js"></script> <!-- footable --> 
<script src="js/plugins.min.js"></script> <!-- plugins --> 
<script src="js/appjs.js"></script> <!-- app --> 

<!-- TODO: why is initialise.js broken? -->
<!-- <script src="js/initialise.js"></script> <1!-- initialise --1> --> 

<script src="js/globaljs.js"></script> <!-- global --> 
<script src="js/homepage.js"></script> <!-- home --> 
<script src="js/landing_page.js"></script> <!-- landingPage --> 
<script src="js/search.js"></script> <!-- search --> 
<script src="js/beforeafter-animation.js"></script> <!-- beforeafter-animation --> 
<script src="js/movie-video.js"></script> <!-- movie-video --> 
<script src="js/font-resizer.js"></script> <!-- font-resizer --> 
<script src="js/jquerymobiletouch.js"></script> <!-- jquery.mobile.touch -->

</head>

<body class="">
<header class="container clearfix header-top">
  <div class="block-group pull-right header-nav">
    <div class="block header-links">
      <ul class="nav nav-pills">
        <li><a href="http://www.ga.gov.au/about/careers">Careers</a></li>
        <li><a href="http://www.ga.gov.au/contact-us">Contact Us</a></li>
        <li><a href="#main-content">Skip to Content</a></li>
      </ul>
    </div>
    <div class="block header-social-media">
      <ul class="nav nav-pills">
        <li><a href="https://twitter.com/GeoscienceAus"><img src="images/twitter.png" alt="Twitter" /></a></li>
        <li><a href="http://www.youtube.com/user/GeoscienceAustralia"><img src="images/youtube.png" alt="YouTube" /></a></li>
      </ul>
    </div>
  </div>
</header>
<hr />
<header class="header-main">
  <div class="header-container">
    <div class="block-group">
      <div class="block" id="header-main-logo"> <a href="http://www.ga.gov.au/home" title="Australian Government - Geoscience Australia"><img src="images/ga-logo.jpg" alt="Australian Government - Geoscience Australia" /></a> </div>
      <div class="block" id="header-main-desc">
        <h1><span>Applying geoscience to Australia's most important challenges</span></h1>
      </div>
      <div id="header-responsive" class="block block-group">
        <button class="block block-group btn-link" id="header-responsive-menu" type="button" title="Click or tap to show or hide the site navigation" aria-hidden="false"> <span class="sr-only">Search</span> <img class="block nav-burger" src="http://www.ga.gov.au/__data/assets/image/0018/16803/burger@2x.png" alt="" /> <img class="block nav-close" src="http://www.ga.gov.au/__data/assets/image/0019/16804/close@2x.png" alt="" /> <span class="block nav-menu-text">Menu</span> </button>
        <button class="block btn-link" id="header-responsive-search" type="button" title="Click or tap to show or hide the search form"> <span class="sr-only">Search</span> <img src="http://www.ga.gov.au/__data/assets/image/0020/16805/search@2x.png" alt="" width="18" /> </button>
      </div>
      <nav id="header-main-nav" class="block drilldown">
        <div class="drilldown-container">
          <ul class="nav nav-pills drilldown-root">
            <li class="home"> <a href="http://www.ga.gov.au/home"> <span class="mobile-only">Home</span> <img class="desktop-only" src="http://www.ga.gov.au/__data/assets/image/0017/16802/home@2x.png" alt="" width="18" /> </a> </li>
            <li class="current"> <a href="http://www.ga.gov.au/about">About</a> </li>
            <li class=""> <a href="http://www.ga.gov.au/scientific-topics">Scientific Topics</a> </li>
            <li class=""> <a href="http://www.ga.gov.au/education">Education</a> </li>
            <li class=""> <a href="http://www.ga.gov.au/data-pubs">Data &amp; Publications</a> </li>
            <li class=""> <a href="http://www.ga.gov.au/news-events">News &amp; Events </a> </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</header>
<div id="body-content"> 
  <!-- Content block, child page inject content -->
  
  <div class="mobile-line-container">&nbsp;</div>
  <div class="search search-subpage">
    <div class="container">
      <link rel="stylesheet" type="text/css" href="css/search-gsa.css" />
      <!-- Google Search GSA Styles --> 
      
      <!--- Google Search GSA --->
      <div class="sb_container_outer">
        <div class="" style="max-height: 95px !important; transition: max-height 284ms;" aria-hidden="true">
          <div class="search-box-wrapper">
            <form id="suggestion_form" name="gs" method="GET" action="http://search.ga.gov.au/search?site=default_collection&client=ga_frontend&output=xml_no_dtd&proxystylesheet=ga_frontend&q=" onsubmit="return (this.q.value == '') ? false : true;">
              <div class="">
                <div class="search-input-wrapper">
                  <input type="text" name="q" class="q" size="32" maxlength="256" value="" autocomplete="off" aria-haspopup="true">
                  <input type="submit" name="btnG" value="Search" class="searchbutton">
                </div>
              </div>
              <input type="hidden" name="client" value="ga_frontend">
              <input type="hidden" name="output" value="xml_no_dtd">
              <input type="hidden" name="proxystylesheet" value="ga_frontend">
              <input type="hidden" name="sort" value="date:D:L:d1">
              <input type="hidden" name="wc" value="200">
              <input type="hidden" name="wc_mc" value="1">
              <input type="hidden" name="oe" value="UTF-8">
              <input type="hidden" name="ie" value="UTF-8">
              <input type="hidden" name="ud" value="1">
              <input type="hidden" name="exclude_apps" value="1">
              <input type="hidden" name="site" value="default_collection">
              <input type="hidden" name="filter" value="0">
              <input type="hidden" name="getfields" value="*">
              <input type="hidden" name="sort">
            </form>
          </div>
        </div>
      </div>
      <!-- End Google Search GSA --> 
      
    </div>
  </div>
  <div class="search-border-container search-border-subpage"></div>
  <div class="container">
    <ol class="breadcrumb">
      <li><a href="http://www.ga.gov.au/home">Home</a></li>
      <li><a href="http://www.ga.gov.au/about">About</a></li>
    </ol>
    <div id="main-content" class="main-content ">
      <h1>AUSGeoid</h1>
    </div>


<%
if (request.getAttribute("fragment") == null) {
    request.setAttribute("fragment", "/WEB-INF/jsp/main.jsp");
}
%>

<jsp:include page="${fragment}" flush="true"/>
    
</div>

<footer id="footer-main" class="footer-global">
  <div class="container">
    <div class="block-group">
      <nav id="footer-main-quicklinks" class="block col-lg-1-4">
        <h3>Quicklinks</h3>
        <ul class="nav">
          <li><a href="http://www.ga.gov.au">Home</a></li>
          <li><a href="http://www.ga.gov.au/about">About</a></li>
          <li><a href="http://www.ga.gov.au/scientific-topics">Scientific Topics</a></li>
          <li><a href="http://www.ga.gov.au/data-pubs">Data &amp; Publications</a></li>
          <li><a href="http://www.ga.gov.au/education">Education</a></li>
          <li><a href="http://www.ga.gov.au/news-events">News &amp; Events</a></li>
        </ul>
      </nav>
      <nav id="footer-main-socialmedia" class="block col-lg-1-4">
        <h3>Connect with us</h3>
        <ul class="nav">
          <li><a href="https://twitter.com/GeoscienceAus"><img src="http://www.ga.gov.au/__data/assets/image/0018/16506/twitter-footer.png" width="35" alt="Link to our Twitter page."></a></li>
          <li><a href="http://www.youtube.com/user/GeoscienceAustralia"><img src="http://www.ga.gov.au/__data/assets/image/0020/16508/youtube-footer.png" width="35" alt="Link to our YouTube page."></a></li>
        </ul>
      </nav>
      <div id="footer-main-address" class="block col-lg-1-4">
        <h3>Our address</h3>
        <p> <b>Geoscience Australia</b><br>
          Cnr Jerrabomberra Ave and<br>
          Hindmarsh Drive<br>
          Symonston ACT 2609 </p>
        <p><a href="https://maps.google.com/maps?q=geoscience+australia&hl=en&sll=37.0625,-95.677068&sspn=56.899383,79.013672&hq=geoscience+australia&radius=15000&t=m&z=13&iwloc=A">View Google Map</a></p>
      </div>
      <div id="footer-main-contact-numbers" class="block col-lg-1-4">
        <h3>Important Numbers</h3>
        <p> General Enquiries: 1800 800 173<br>
          Switchboard: +61 2 6249 9111<br>
          Earthquake information: 1800 655 739<br>
          Media Hotline: 1800 882 035 </p>
        <p> <a href="http://www.ga.gov.au/contact-us" class="btn btn-default">Contact us</a> </p>
      </div>
    </div>
  </div>
  <br>
  <hr>
  <br>
  <div class="container">
    <div class="block-group">
      <div id="footer-bottom-logo" class="block col-lg-1-4"> <a href="http://creativecommons.org/licenses/by/4.0/legalcode" target="_blank"><img src="http://www.ga.gov.au/__data/assets/image/0015/20139/by.png" alt="Creative Commons Logo" width="88"></a> </div>
      <nav id="footer-bottom-nav" class="block col-width-auto">
        <ul class="nav nav-pills">
          <li>&copy; Commonwealth of Australia</li>
          <li><a href="http://www.ga.gov.au/copyright">Copyright</a></li>
          <li><a href="http://www.ga.gov.au/privacy">Privacy</a></li>
          <li><a href="http://www.ga.gov.au/accessibility">Accessibility</a></li>
          <li><a href="http://www.ga.gov.au/sitemap">Sitemap</a></li>
          <li><a href="http://www.ga.gov.au/ips">Information Publication Scheme</a></li>
          <li><a href="http://www.ga.gov.au/ips/foi">Freedom of Information</a></li>
        </ul>
      </nav>
    </div>
  </div>
</footer>

<script type="text/javascript">
	$(function() {
		$('#tabs').tabs();
	});
</script>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css">

</body>
</html>
