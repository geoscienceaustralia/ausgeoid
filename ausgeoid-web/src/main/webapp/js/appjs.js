var geo = (function () {
  
  // Private variables and functions are declared here
  

  return {
    // Public variables and functions are declared here
    config: {},
    lang: {},
    plugins: {},
    controllers: {},

    init: function () {
      var controllers = geo.controllers;
      for (var key in controllers) {
        var controller = controllers[key];
        if (controller.hasOwnProperty("init")) {
          if (controller.hasOwnProperty("config") && controller.config.hasOwnProperty("el") && controller.config.el.length && !($(controller.config.el).length)) {
            this.log("geo.init: Controller: " + key + " not initialized as selector " + controller.config.el + " was not found on page.");
          } else {
            controller.init();
            this.log("geo.init: Controller: " + key + " initialised.");
          }
        } else {
          this.log("geo.init: Controller: " + key + " has no init function.");
        }
      }
    },

    log: function (message) {
      if (typeof(console) !== "undefined") {
        console.log(message);
      }
    },

    keys: Object.keys || (function () {
      var hasOwnProperty = Object.prototype.hasOwnProperty,
          hasDontEnumBug = !{toString:null}.propertyIsEnumerable("toString"),
          DontEnums = [
              'toString',
              'toLocaleString',
              'valueOf',
              'hasOwnProperty',
              'isPrototypeOf',
              'propertyIsEnumerable',
              'constructor'
          ],
          DontEnumsLength = DontEnums.length;

      return function (o) {
          if (typeof o != "object" && typeof o != "function" || o === null)
              throw new TypeError("Object.keys called on a non-object");

          var result = [];
          for (var name in o) {
              if (hasOwnProperty.call(o, name))
                  result.push(name);
          }

          if (hasDontEnumBug) {
              for (var i = 0; i < DontEnumsLength; i++) {
                  if (hasOwnProperty.call(o, DontEnums[i]))
                      result.push(DontEnums[i]);
              }
          }

          return result;
      };
    })()
  }
}());