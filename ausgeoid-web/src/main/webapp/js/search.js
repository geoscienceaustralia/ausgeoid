geo.controllers.search = (function () {
  // Private variables and functions are declared here

  function getURLParams() {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    //if first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = pair[1];
    //if second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]], pair[1] ];
      query_string[pair[0]] = arr;
    //if third or later entry with this name
    } else {
      query_string[pair[0]].push(pair[1]);
      }
    } 
    return query_string;
  }

  function sortInit() {
    //initialise sort links
    urlParams = getURLParams();
    if( typeof(urlParams.news_items) != "undefined" ) {
      $('#search-sort-news').attr('checked',true);
    }
    if( typeof(urlParams.search_page_18170_sort_by) === "undefined" || urlParams.search_page_18170_sort_by === '0' ) {
      $('#search-sort-title-relevance').attr('checked',true);
    }
    if( typeof(urlParams.search_page_18170_sort_direction) != "undefined" && urlParams.search_page_18170_sort_by === '1' && urlParams.search_page_18170_sort_direction === '0' ) {
      $('#search-sort-title-asc').attr('checked',true);
    }
    if( typeof(urlParams.search_page_18170_sort_direction) != "undefined" && urlParams.search_page_18170_sort_by === '1' && urlParams.search_page_18170_sort_direction === '1' ) {
      $('#search-sort-title-dsc').attr('checked',true);
    }
    $('#search-sort-title-relevance').change(function() {
      if( $(this).is(':checked') ) {
        $('#search_page_18170_sort_by').val(0);
        $('#search_page_18170_sort_direction').val(1);
        if( !$('#search-sort-news').is(':checked') ) {
          $('#news_items').remove();
        }
        $('#search_page_18170').submit();
      }
    });
    $('#search-sort-title-asc').click(function(e) {
      if( $(this).is(':checked') ) {
        $('#search_page_18170_sort_by').val(1);
        $('#search_page_18170_sort_direction').val(0);
        if( !$('#search-sort-news').is(':checked') ) {
          $('#news_items').remove();
        }
        $('#search_page_18170').submit();
      }
    });
    $('#search-sort-title-dsc').click(function(e) {
      if( $(this).is(':checked') ) {
        $('#search_page_18170_sort_by').val(1);
        $('#search_page_18170_sort_direction').val(1);
        if( !$('#search-sort-news').is(':checked') ) {
          $('#news_items').remove();
        }
        $('#search_page_18170').submit();
      }
    });
    $('#search-sort-news').change(function() {
      if( $(this).is(':checked') ) {
        $('#news_items').val(1604);
        $('#search_page_18170').submit();
      } else {
        $('#news_items').remove();
        $('#search_page_18170').submit();
      }
    });
  }

  return {
    // Public variables and functions are declared here
    config: {
      el: ".matrix-sort"
    },

    init: function () {
      sortInit();
    }
  }
}());