geo.controllers.movie = (function () {

  return {
    // Public variables and functions are declared here
    config: {
      el: "#movie-video"
    },

    init: function () {
      jwplayer("movie-video").setup({
        flashplayer: flashPlayer,
        file: videoUrl,
        image: coverImage,
        width: "100%",
        aspectratio: "16:9",
        logo: {
          hide: true
        }

      });
    }
  }
}());