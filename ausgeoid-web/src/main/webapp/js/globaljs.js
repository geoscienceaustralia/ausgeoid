geo.controllers.global = (function () {
  // Private variables and functions are declared here

  function skipTwitterFeed() {
    $('#skip-twitter-feed').keydown(function(event) {
      event.preventDefault();
      $('#home-ga-tv-feature').focus();
    });
  }

  function getURLParams() {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    //if first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = pair[1];
    //if second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]], pair[1] ];
      query_string[pair[0]] = arr;
    //if third or later entry with this name
    } else {
      query_string[pair[0]].push(pair[1]);
      }
    } 
    return query_string;
  }

  function toggleSearchField() {
    var currentValue = $('#queries_keywords_query').val();
    $('#queries_keywords_query').blur(function() {
      if($(this).val() !== '') {
        currentValue = $('#queries_keywords_query').val();
      }
    });
  }

  function selectSearchType() {
    var searchType = getURLParams();
    if(searchType['topic'] === 'navigator') {
      $('#topic').val('navigator');
      $('#topic').trigger('change');
    }
  }

  function searchInit() {
    $('#topic').change(function() {
      if( $('#topic').val() === 'matrix' ) {
        $('#site-search').attr('action',$('#website-search-path').val());
      } else {
        $('#site-search').attr('action',$('#navigator-search-path').val());
      }
    });
  }
  
  function lightboxInit() {
    $('.lightbox').each(function() {
      $(this).attr('rel','gallery');
    });
    $(".lightbox").fancybox({
      helpers: {
        title : {
          type : 'inside'
        }
      }
    });
  }

  function enhanceTables() {
    $('.main-content table tr:even').addClass('even-row');
    $('.footable').footable({
      breakpoints: {
        tiny: 100,
        medium: 555,
        big: 2048
      }
    }); 
  }

  function responsiveMenu() {
    var initialWindowWidth = $(window).width();
    var nav = responsiveNav("#header-main-nav", {
      customToggle: "#header-responsive-menu",
      openPos: 'relative',
      close: function() {
        $(".drilldown").drilldown("reset");
      }
    });
    var navsearch = responsiveNav(".search", {
      customToggle: "#header-responsive-search",
      openPos: 'absolute',
      navClass: 'search'
    });

    function initDrilldownSub() {
      if($("#header-responsive").is(":hidden")) {
        $(".megamenu").removeClass("drilldown-sub");
      } else {
        $(".megamenu").addClass("drilldown-sub");
      }
    };

    $(window).resize( $.throttle( 250, function() {
      //make sure we calculate the screen width then decide if we need to hide search nav
      var win = $(this);
      clearTimeout(winResizeDetect);
      var winResizeDetect = setTimeout(function() {
        var hasWindowResized = (win.width() != initialWindowWidth);
        if(hasWindowResized) {
          initialWindowWidth = win.width();
          navsearch.close();
          nav.close();
          initDrilldownSub();
        }
      });
    }));

    $(".drilldown").drilldown({ speed: 0 });
    $(window).trigger("resize");

    initDrilldownSub();
  }

  function injectTableOfContents() {
    //injects table of contents into standard pages based on h2 tags (can be switched off via metadata)
    if( $('#page-toc').length ) {
      var headerIDCounter = 1;
      var links = [];
      $('.main-content h2').each(function() {
        $(this).attr('id','heading-' + headerIDCounter);
        links.push('<li><a href="#'+$(this).attr('id')+'">'+$(this).text()+'</a></li>');
        headerIDCounter ++;
      });
      if( links.length ) {
        $('#page-toc').append('<p>Contents</p><ul />');
        for(var i=0; i< links.length; i++) {
          $('#page-toc ul').append(links[i]);
          $('#page-toc').show();
        }
      } else {
        $('#page-toc').remove();
      }
    }
  }

  function removeBracketsFromNav() {
    //removes the brackets from pages that are under construction (remove for go live)
    $('#header-main-nav li > a, .two-columns-page-nav a span').each(function() {
      var theText = $.trim($(this).text());
      if (theText === "Home") return;
      theText = theText.replace(/\(\( /g,'');
      theText = theText.replace(/ \)\)/g,'');
      $(this).text(theText);
    });


    $("#header-main-nav h3").each(function() {
       var t = $(this).text();

       if (t.indexOf("(") > -1) {
        t = t.split("(").join("");
        t = t.split(")").join("");
       }

       $(this).text(t);
    });
  };

  function enhanceMainNav() {
    //adds the down arrow for main nav items that have child pages & clear floats
    $('.drilldown-root > li').each(function() {
      if( $('.megamenu', this).length ) {
        $(this).addClass('has-children');
      }
    });
    $('.megamenu .block:nth-child(5n+6)').addClass('clear-block');
    $(".has-children").mouseenter(function(){
    	var $this = $(this);
      if($("#header-responsive-search").not(":visible")){
        var offsetLeft = $(this).offset().left;
        $this.find(".megamenu").css({ width: $(window).width(), position: "absolute", left: -offsetLeft });
      } else {
        $(".megamenu").offset({ left: 0 });
        $(".megamenu").css("width", auto);
      }
    });

    $(window).resize( $.debounce( 250, function() {
      var nav = $("#header-main-nav").width();

      if($("#header-responsive").is(":hidden")) {
        $("#header-main-nav .drilldown-root > li ").each(function() {
          nav -= $(this).width();
        });

        $("#header-main-nav .drilldown-root > li").css({marginLeft: Math.floor(nav/5) - 1});
      } else {
        $("#header-main-nav .drilldown-root > li").css({marginLeft: 0});
      }
    }));
    $(window).trigger("resize");
  }

  function enhanceLeftNav() {
    if ($("#left-nav").length) {
      $("#left-nav > li > a").addClass("clearfix");

      $(window).resize( $.throttle( 250, function() {
        // hide left navigation
        if($("#header-responsive-search").is(":hidden")) {
          $("#left-nav").removeClass("dropdown-menu");
          $("#left-nav-toggle").prop("disabled", false);
          $("#left-nav").addClass("nav");
        } else {
          $("#left-nav").addClass("dropdown-menu");
          $("#left-nav-toggle").prop("disabled", false);
          $("#left-nav").removeClass("nav");
        }
      }));
    }
  }

  function findExternalLink() {
    $(".subpage #main-content a[href*='://']:not([href*='" + window.location.hostname.replace("www.", "") + "'])").each(function(index, item) { 
      var $item = $(item);
      
      if($.trim($item.text()).length) {
       $item
          .addClass("external-link")
          .attr("target","_blank"); 
      }
    });
  }

  return {
    config: {
      el: "body"
    },

    init: function () {
      enhanceMainNav();
      enhanceLeftNav();
      injectTableOfContents(); 
      responsiveMenu(); 
      enhanceTables();
      lightboxInit();
      searchInit();
      selectSearchType();
      toggleSearchField();
      //skipTwitterFeed();
      findExternalLink();
    }
  }
}());