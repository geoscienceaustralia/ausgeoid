geo.controllers.landingPage = (function () {
  // Private variables and functions are declared here

  function enhanceLayoutGrid() {
    //clear floats
    $('.content-grid-scientific .primary-landing-page-block:nth-child(4n+5)').addClass('clear-block');
  } 

  return {
    // Public variables and functions are declared here
    config: {
      el: ".content-grid-scientific"
    },

    init: function () {
      enhanceLayoutGrid();  
    }
  }
}());