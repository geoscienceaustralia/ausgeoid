geo.controllers.fontResizer = (function () {
  return {
    config: {
      el: "body"
    },

    init: function () {
      // bind the template
      $(".header-links").after( '<div class="block header-font-resizer">' +
                                  '<button type="button" id="font-resizer-smaller" class="btn-link"><small>A</small></button>' +
                                  '<button type="button" id="font-resizer-larger" class="btn-link">A</button' +
                                "</div>");

      // font resize script
      fontResize = {
        options: {
          maxStep: 1.2,
          defaultFS: 0.813,
          resizeStep: 0.062,
          resizeHolder: 'body',
          cookieName: 'fontResizeCookie'
        },
        init: function() {
          this.domReady(function(){
            this.setDefaultScaling();
            this.addDefaultHandlers();
          });
          
          return this;
        },
        addDefaultHandlers: function() {
          this.addHandler('font-resizer-larger', 'inc');
          this.addHandler('font-resizer-smaller', 'dec');
        },
        setDefaultScaling: function() {
          if(this.options.resizeHolder == 'html') { 
            this.resizeHolder = document.documentElement; 
          } else {
            this.resizeHolder = document.body; 
          }
          
          var cSize = this.getCookie(this.options.cookieName);
          if(cSize) {
            this.fSize = parseFloat(cSize,10)
          } else {
            this.fSize = this.options.defaultFS;
          }

          this.changeSize();
        },
        changeSize: function(direction) {
          if(typeof direction !== 'undefined') {
            if(direction == 1) {
              this.fSize += this.options.resizeStep;
              
             if (this.fSize > this.options.defaultFS * this.options.maxStep) this.fSize = this.options.defaultFS * this.options.maxStep;
            } else if(direction == -1) {
              this.fSize -= this.options.resizeStep;
              
              if (this.fSize < this.options.defaultFS / this.options.maxStep) this.fSize = this.options.defaultFS / this.options.maxStep;
            } else {
              this.fSize = this.options.defaultFS;
            }
          }
        
          this.resizeHolder.style.fontSize = this.fSize + 'em';
          this.updateCookie(this.fSize.toFixed(2));
          return false;
        },
        addHandler: function(obj, type) {
          if(typeof obj === 'string') { obj = document.getElementById(obj); }
          if(typeof obj !== 'undefined') {
            switch (type) {
              case 'inc':
                obj.onclick = this.bind(this.changeSize,this, [1]);
                break;
              case 'dec':
                obj.onclick = this.bind(this.changeSize,this, [-1]);
                break;
              default:
                obj.onclick = this.bind(this.changeSize,this, [0]);
                break;
            }
          }
        },
        domReady: function(fn) {
          var scope = this, calledFlag;
          (function() {
              if (document.addEventListener) {
                document.addEventListener('DOMContentLoaded', function(){
                  if(!calledFlag) { calledFlag = true; fn.call(scope); }
          }, false)
              }
          
            if (!document.readyState || document.readyState.indexOf('in') != -1) {
              setTimeout(arguments.callee, 9);
            } else {
              if(!calledFlag) { calledFlag = true; fn.call(scope); }
            }
          }());
        },
        updateCookie: function(scaleLevel) {
          this.setCookie(this.options.cookieName,scaleLevel);
        },
        getCookie: function(name) {
          var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.jQuery?*|{}\(\)\[\]\\\/\+^])/g, '\\jQuery1') + "=([^;]*)"));
          return matches ? decodeURIComponent(matches[1]) : undefined;
        },
        setCookie: function(name, value) {
          var exp = new Date();
          exp.setTime(exp.getTime()+(30*24*60*60*1000));
          document.cookie = name + '=' + value + ';' +'expires=' + exp.toGMTString() + ';' +'path=/';
        },
        bind: function(fn, scope, args){
          return function() {
            return fn.apply(scope, args || arguments);
          }
        }
      }.init();
    }
  }
}());