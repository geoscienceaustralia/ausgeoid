geo.controllers.homepage = (function () {
  function tabInit(){
    //clear floats
    $('.content-grid-scientific .primary-landing-page-block:nth-child(5)').addClass('clear-block');
  }

  var enhanceCarousel = function() {
    $(".carousel-indicators").append('<li class="carousel-indicators-control"><span class="glyphicon glyphicon-pause"></span>&nbsp;<span class="text">Pause</span></li>');

    $("#carousel-16877").swiperight(function() {
      $(this).carousel('prev');
    });
    $("#carousel-16877").swipeleft(function() {
      $(this).carousel('next');
    });

    $("#homepage-carousel").on("click", ".carousel-indicators-control", function() {
      var $control = $(this),
          $controlText = $(this).find(".text"),
          text = $controlText.text();
      if (text == "Pause") {
        $controlText.text("Play");
        $control.find(".glyphicon").toggleClass("glyphicon-pause");
        $control.find(".glyphicon").toggleClass("glyphicon-play");

        $('#carousel-16877').carousel('pause');
      } else {
        $controlText.text("Pause");
        $control.find(".glyphicon").toggleClass("glyphicon-pause");
        $control.find(".glyphicon").toggleClass("glyphicon-play");

        $('#carousel-16877').carousel('cycle');
      }
    });
  };

  return {
    // Public variables and functions are declared here
    config: {
      el: "#homepage-carousel"
    },

    init: function () {
      enhanceCarousel();
    }
  }
}());