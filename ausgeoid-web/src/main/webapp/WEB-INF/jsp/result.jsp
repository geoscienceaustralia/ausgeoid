<%@ page import="au.gov.ga.gemd.geoid.core.domain.GeoidValues" %>
<%@ page import="au.gov.ga.gemd.geoid.core.domain.Interpolant" %>

<%!
public String coordinateFormatPP(String format) {
    if (format.equals("DD")) {
        return "decimal degrees";
    } if (format.equals("DMS")) {
        return "degrees minutes seconds";
    } else {
        return "unknown format";
    }
}
%>

<%
Interpolant interpolant = (Interpolant) request.getAttribute("interpolant");
String coordinateFormat = coordinateFormatPP(interpolant.getDegreeType());
%>

<h2>Calculation Results</h2>
<table>
    <tr>
        <td>AUSGeoid file used:</td>
        <td>${interpolant.ausGeoid.name}</td>
    </tr>
    <tr>
        <td>Latitude (<%=coordinateFormat%>):</td>
        <td>${interpolant.latitude}</td>
    </tr>
    <tr>
    <td>Longitude (<%=coordinateFormat%>):</td>
        <td>${interpolant.longitude}</td>
    </tr>
    <tr>
        <td>AHD height (m):</td>
        <td>${geoidValues.orthometricHeightString}</td>
    </tr>
    <tr>
        <td>Ellipsoidal height (m):</td>
        <td>${geoidValues.ellipsoidalHeightString}</td>
    <tr>
    <tr>
        <td>AUSGeoid value (m):</td>
        <td>${geoidValues.NValueString}</td>
    </tr>
    <tr>
        <td>AUSGeoid uncertainty (m):</td>
<%
String uncertainty = ((GeoidValues) request.getAttribute("geoidValues")).getUncertaintyString();
%>
        <td><%= uncertainty != null ? uncertainty : "-" %></td>
    </tr>
        <td>Deflection prime meridian (seconds):</td>
        <td>${geoidValues.deflectionPrimeMeridianString}</td>
    </tr>
    <tr>
        <td>Deflection prime vertical (seconds):</td>
        <td>${geoidValues.deflectionPrimeVerticalString}</td>
    </tr>
</table>
<br/>
<p class="comments">Comments &amp; Suggestions to <a href='mailto:geodesy@ga.gov.au?subject=AUSGEOID09'>geodesy@ga.gov.au</a></p>
