<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="au.gov.ga.gemd.geoid.core.domain.AusGeoid" %>
<%@ page import="au.gov.ga.gemd.ausgeoid.web.form.InputForm" %>

<img src="images/1per250kFiles.PNG" alt="AUSGEOID09 Map" style="float: right; margin: 0px 0 10px 10px;" />
<!--start your content here-->

<script type="text/javascript">
//<![CDATA[
function showDDHint() {
    $("#dmsHint").hide()
    $("#ddHint").show()
}

function showDMSHint() {
    $("#ddHint").hide()
    $("#dmsHint").show()
}

$(document).ready(function() {
    showDDHint();
});
//]]>
</script>

<p>AUSGeoid models are used convert ellipsoid heights to <a href="http://www.ga.gov.au/scientific-topics/positioning-navigation/geodesy/ahdgm/ahd">Australian Height Datum (AHD)</a> heights and vice versa.</p>

<p>The tools provided below can be used to convert your data using AUSGeoid98, AUSGeoid09 and AUSGeoid2020 interactively (left tab) or to submit a file to process multiple points at once (right tab).</p>

<p><strong>Note:</strong> Upper limit of batch processing is 5000 points.</p>

<p>For the first time, AUSGeoid2020 provides uncertainty along with the ellipsoid to AHD value. Uncertainty is not available for the AUSGeoid98 and AUSGeoid09 models.</p>

<h3>AUSGeoid Model Differences</h3>
<ul>
    <li>AUSGeoid98: ellipsoidal to gravimetric geoid values (offsets with respect to AHD can be up to 0.5 m)</li>
    <li>AUSGeoid09: ellipsoidal to AHD values</li>
    <li>AUSGeoid2020: ellipsoidal to AHD values with uncertainty</li>
</ul>

<p>For more information, please refer to <a href="http://www.ga.gov.au/webtemp/image_cache/GA16650.pdf">Brown, 2010</a> and <a href="https://d28rz98at9flks.cloudfront.net/72046/72046_bigobjversion.pdf">Brown et al., 2011.</a></p>

<h3>AUSGeoid Application</h3>

<div id="tabs">
<ul>
    <li><a href="#tabs-1">Compute an AUSGeoid value online</a></li>
    <li><a href="#tabs-2">Batch Processing</a></li>
    </ul>
    
    <div id="tabs-1">
    <p>Enter your data in the fields below.</p>
    <p style="margin: 0px;">AUSGeoid98 extents are: Latitude [-8 and -46] Longitude [108 and 160].</p>
    <p style="margin: 0px;">AUSGeoid09 extents are: Latitude [-8 and -46] Longitude [108 and 160].</p>
    <p>AUSGeoid2020 extents are: Latitude [-8 and -61] Longitude [93 and 174].</p>

    <p><strong>Note:</strong> Only GDA94 coordinates should be used with AUSGeoid98 and AUSGeoid09. Only GDA2020 coordinates should be used with AUSGeoid2020.</p>
    
    <form method="post" action="/ausgeoid/comp.html">
    <table summary="Table layout for AUSGeoid form data request" style="white-space:nowrap" cellspacing="2" cellpadding="4" border="0">
    <tr>
        <td><strong>AUSGeoid:</strong></td>
    </tr>
    <tr>
        <td>
            <select name="<%=InputForm.AUS_GEOID%>">
<%
AusGeoid[] ausgeoids = AusGeoid.values();
request.setAttribute("ausgeoids", ausgeoids);
%>
<%
for (int i = 0; i < ausgeoids.length; i++) {
    String logicalName = ausgeoids[i].name();
    String displayName = ausgeoids[i].getName();
%>
                <option value="<%=logicalName%>"><%=displayName%></option>
<%
}
%>
            </select>
        </td>
    </tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
        <td width="100" align="left"><strong>GDA94/2020 Latitude:</strong></td>
        <td width="100" align="left"><strong>GDA94/2020 Longitude:</strong></td>
    </tr>
    <tr>
        <td><input name="latitude" size="15" /></td>
        <td><input name="longitude" size="15" /></td>
    </tr>
    <tr id="ddHint">
       <td id="latHint">e.g., -35.5000000</td>
       <td id="lonHint">145.5000000</td>
    </tr>
    <tr id="dmsHint">
       <td id="latHint">e.g., -35.3000</td>
       <td id="lonHint">145.3000 (DDD.MMSS)</td>
    </tr>
    <tr>
        <td><input id="degreeType_rb1" type="radio" name="degreeType" value="DD" checked="checked" onchange="showDDHint()"/>Decimal Degrees</td>
    </tr>
    <tr>
        <td><input id="degreeType_rb2" type="radio" name="degreeType" value="DMS" onchange="showDMSHint()"/>Degrees Minutes Seconds</td>
    </tr>

	<tr><td>&nbsp;</td></tr>
    <tr> <td width="100" align="left"><strong>Height (m):</strong></td> </tr>
    <tr>
        <td><input name="height" size="15" /></td>
        <td/>
    </tr>
    <tr> <td>e.g., 12.345</td> </tr>
    <tr>
        <td><input id="heightType_rb1" type="radio" name="heightType" value="EH" checked="checked"/>Ellipsoidal</td>
    </tr>
    <tr>
        <td><input id="heightType_rb2" type="radio" name="heightType" value="AH"/>AHD</td>
    </tr>
    <tr>
        <td colspan="3"> <input type="submit" value ="compute" /><input type="reset" value ="reset" /></td>
    </tr>
    </table>

    </form>

    </div>

    <div id="tabs-2"> 

    <p>Upload your data file below. The format required is Comma Separated Version (.csv).</p>

    <p style="margin: 0px;"><strong>Sample input data (decimal degrees):</strong></p>
    <p style="margin: 0px;">1,-35.5000000,145.5000000,12.000</p>
    <p style="margin: 0px;">2,-36.7500000,146.7500000,-2.500</p>
    <p>...</p>

    <p style="margin: 0px;"><strong>Sample input data (degrees minutes seconds):</strong></p>
    <p style="margin: 0px;">1,-35.3000,145.3000,12.000</p>
    <p style="margin: 0px;">2,-36.4500,146.4500,-2.500</p>
    <p>...</p>

    <p style="margin: 0px;"><strong>Note:</strong></p>
    <p style="margin: 0px;">AUSGeoid98 extents are: Latitude [-8 and -46] Longitude [108 and 160].</p>
    <p style="margin: 0px;">AUSGeoid09 extents are: Latitude [-8 and -46] Longitude [108 and 160].</p>
    <p>AUSGeoid2020 extents are: Latitude [-8 and -61] Longitude [93 and 174].</p>
    
    <p>Output format is point id, latitude, longitude, height (input), AHD height, ellipsoidal height, AUSGeoid value, AUSGeoid uncertainty, DoV PM, DoV PV.</p>
    
    <form id="uploadForm" method="post" action="/ausgeoid/trans.html"
        enctype="multipart/form-data">

    <table>
        <tbody>
			<tr>
				<td><strong>AUSGeoid:</strong></td>
            </tr>
			<tr>
				<td>
					<select name="<%=InputForm.AUS_GEOID%>">
<%
for (int i = 0; i < ausgeoids.length; i++) {
	String logicalName = ausgeoids[i].name();
	String displayName = ausgeoids[i].getName();
%>
						<option value="<%=logicalName%>"><%=displayName%></option>
<%
}
%>
					</select>
				</td>
			</tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td><strong>Input Type:</strong></td>
            </tr>
            <tr>
                <td> <input id="degreesType_rb1" type="radio" name="degreeType" value="DD" checked="checked" />Decimal Degrees </td> 
                <td><input id="heightType_rb1" type="radio" name="heightType" value="EH" checked="checked" />Ellipsoidal Height</td>
            </tr>
            <tr>
                <td> <input id="degreesType_rb2" type="radio" name="degreeType" value="DMS"/>Degrees Minutes Seconds </td>
                <td><input id="heightType_rb2" type="radio" name="heightType" value="AH"/>AHD Height</td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td><input id="inputFile" name="inputFile" type="file" /></td>
                <td></td>
            </tr>
            <tr>
                <td><input type="submit" value="Upload" /></td>
            </tr>
        </tbody>
    </table>
    </form>

    </div>

</div>

<h3>Download AUSGeoid files</h3>

<p>The AUSGeoid files in text file format and binary format (NTv2) can be downloaded from <a href="https://s3-ap-southeast-2.amazonaws.com/geoid/">AWS S3</a>. To download a file, cut and paste the bucket path (https://s3-ap-southeast-2.amazonaws.com/geoid/) into a browser and append the file name you would like to download (e.g. AUSGeoid2020_20170908.gsb) and tap Enter.</p>
