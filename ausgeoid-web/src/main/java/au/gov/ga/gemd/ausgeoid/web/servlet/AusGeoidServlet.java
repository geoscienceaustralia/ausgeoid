package au.gov.ga.gemd.ausgeoid.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.gov.ga.gemd.ausgeoid.web.form.InputForm;
import au.gov.ga.gemd.geoid.core.GeoidInterpolationException;
import au.gov.ga.gemd.geoid.core.domain.GeoidValues;
import au.gov.ga.gemd.geoid.core.domain.Interpolant;

@SuppressWarnings("serial")
public class AusGeoidServlet extends AbstractAusGeoidServlet {
    private static Logger logger = LoggerFactory.getLogger(AusGeoidServlet.class.getName());
    
    protected void perform(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        logger.debug("perform Start");

        try {
            InputForm inputForm = new InputForm();
            
            inputForm.put(InputForm.AUS_GEOID, request.getParameter(InputForm.AUS_GEOID));
            inputForm.put(InputForm.LATITUDE, request.getParameter(InputForm.LATITUDE));
            inputForm.put(InputForm.LONGITUDE, request.getParameter(InputForm.LONGITUDE));
            inputForm.put(InputForm.DEGREE_TYPE, request.getParameter(InputForm.DEGREE_TYPE));
            inputForm.put(InputForm.HEIGHT, request.getParameter(InputForm.HEIGHT));
            inputForm.put(InputForm.HEIGHT_TYPE, request.getParameter(InputForm.HEIGHT_TYPE));
            this.validator.validate(inputForm);

            Interpolant interpolant = buildInput(inputForm);

            GeoidValues geoidValues = this.geoidAdapter.interpolate(interpolant);

            logger.debug("geoidValues.getNValue(): " + geoidValues.getNValue());
            logger.debug("geoidValues.getNValue(): " + geoidValues.getDeflectionPrimeMeridian());
            logger.debug("geoidValues.getNValue(): " + geoidValues.getDeflectionPrimeVertical());

            request.setAttribute("geoidValues", geoidValues);
            request.setAttribute("interpolant", interpolant);

            request.setAttribute("fragment", RESULT_FRAGMENT);
            forward(request, response, INDEX_PAGE);
        } catch (GeoidInterpolationException ex) {
            handleError(request, response, ex);

        } catch (Throwable thr) {
            handleFatal(request, response, thr);
        }
    }
}
