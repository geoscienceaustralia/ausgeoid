package au.gov.ga.gemd.ausgeoid.web.form;

import java.util.HashMap;

public class InputForm extends HashMap <String, String> {

    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String HEIGHT = "height";
    public static final String HEIGHT_TYPE = "heightType";
    public static final String DEGREE_TYPE = "degreeType";
    public static final String AUS_GEOID = "ausgeoid";
    
    public InputForm () {
        super ();   
    }
    // TODO JRF REMOVE private Map<String, String> parameterMap;
    
    //public InputForm(Map<String, String> params) {
        //this.parameterMap = params;
    //}
    
    //public Map <String, String>
    //getParameterMap () {
        //return (parameterMap);
        
    //}
    
    //public String get(String fieldName) {
        //return this.parameterMap.get(fieldName);
    //}
    
}
