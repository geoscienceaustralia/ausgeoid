package au.gov.ga.gemd.ausgeoid.web.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.gov.ga.gemd.ausgeoid.web.GeoidValidationException;
import au.gov.ga.gemd.ausgeoid.web.form.InputForm;
import au.gov.ga.gemd.ausgeoid.web.validator.GeoidValidator;
import au.gov.ga.gemd.geoid.core.GeoidAdapter;
import au.gov.ga.gemd.geoid.core.GeoidAdapterFactory;
import au.gov.ga.gemd.geoid.core.domain.AusGeoid;
import au.gov.ga.gemd.geoid.core.domain.Interpolant;

@SuppressWarnings("serial")
public abstract class AbstractAusGeoidServlet extends HttpServlet {

    protected static final String INDEX_PAGE = "/index.jsp";
    protected static final String MAIN_FRAGMENT = "/WEB-INF/jsp/main.jsp";
    protected static final String RESULT_FRAGMENT = "/WEB-INF/jsp/result.jsp";
    protected static final String ERROR_FRAGMENT = "/WEB-INF/jsp/error.jsp";
    protected static final String FATAL_FRAGMENT = "/WEB-INF/jsp/fatal.jsp";

    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected GeoidAdapter geoidAdapter;

    protected GeoidValidator validator;

    @Override
    public void init() throws ServletException {
        this.geoidAdapter = GeoidAdapterFactory.getInstance();
        this.validator = new GeoidValidator();
    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        perform(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        perform(request, response);
    }

    protected abstract void perform(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException;

    protected void generateFatalPage(HttpServletRequest request, Throwable thr)
            throws IOException {
        if (this.logger.isDebugEnabled()) {
            String stackTrace = ExceptionUtils.getStackTrace(thr);
            request.setAttribute("exception", stackTrace);
        }
    }

    protected void generateErrorPage(HttpServletRequest request, Exception ex)
            throws IOException {

        String msg = ex.getMessage() != null ? ex.getMessage() : "";

        StringBuilder errorMessage = new StringBuilder(msg);

        if (ex instanceof GeoidValidationException) {

            GeoidValidationException e = (GeoidValidationException) ex;
            List<String> messages = e.getMessages();

            errorMessage = new StringBuilder();

            if (messages != null && !messages.isEmpty()) {

                errorMessage.append("<ul>");

                for (String message : messages) {
                    errorMessage.append("<li>");
                    errorMessage.append(message);
                    errorMessage.append("</li>");
                }

                errorMessage.append("</ul>");
            }

        }
        request.setAttribute("message", errorMessage.toString());
    }

    protected void forward(HttpServletRequest request,
            HttpServletResponse response, String path) throws ServletException,
            IOException {
        RequestDispatcher dispatcher = getServletContext()
                .getRequestDispatcher(path);
        dispatcher.forward(request, response);
    }

    protected void handleError(HttpServletRequest request,
            HttpServletResponse response, Exception ex) throws IOException,
            ServletException {
        // Generate error page
        generateErrorPage(request, ex);
        request.setAttribute("fragment", ERROR_FRAGMENT);
        forward(request, response, INDEX_PAGE);
    }

    protected void handleFatal(HttpServletRequest request,
            HttpServletResponse response, Throwable thr) throws IOException,
            ServletException {
        // Log error
        this.logger.error(thr.getMessage(), thr);

        // Generate fatal page
        generateFatalPage(request, thr);
        request.setAttribute("fragment", FATAL_FRAGMENT);
        forward(request, response, INDEX_PAGE);
    }

    protected Interpolant buildInput(Map <String, String> inputMap) {
        Interpolant interpolant = new Interpolant();
    
        if (inputMap.containsKey(InputForm.AUS_GEOID)) {
            interpolant.setAusGeoid(AusGeoid.valueOf(inputMap.get(InputForm.AUS_GEOID)));
        }
        if (inputMap.containsKey(InputForm.LATITUDE)) {
            interpolant.setLatitude(Double.parseDouble(inputMap.get(InputForm.LATITUDE)));
        }
        if (inputMap.containsKey(InputForm.LONGITUDE)) {
            interpolant.setLongitude(Double.parseDouble(inputMap.get(InputForm.LONGITUDE)));
        }
        if (inputMap.containsKey(InputForm.HEIGHT)) {
            interpolant.setHeight(Double.parseDouble(inputMap.get(InputForm.HEIGHT)));
        }
        interpolant.setDegreeType (inputMap.get(InputForm.DEGREE_TYPE));
        interpolant.setHeightType (inputMap.get(InputForm.HEIGHT_TYPE));

        return interpolant;
    }
}
