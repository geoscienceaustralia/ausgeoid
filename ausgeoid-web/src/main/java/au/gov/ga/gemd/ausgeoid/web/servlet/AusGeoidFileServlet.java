package au.gov.ga.gemd.ausgeoid.web.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import au.gov.ga.gemd.ausgeoid.web.form.InputForm;
import au.gov.ga.gemd.geoid.core.GeoidInterpolationException;
import au.gov.ga.gemd.geoid.core.domain.GeoidValues;
import au.gov.ga.gemd.geoid.core.domain.InputType;
import au.gov.ga.gemd.geoid.core.domain.Interpolant;

@SuppressWarnings("serial")
public class AusGeoidFileServlet extends AbstractAusGeoidServlet {
    
    private static final String ACCEPTED_FILE_EXTENSIONS_KEY = "geoid.acceptedFileExtensions";
    
    private static final String COMMA = ",";
    
    private static final String[] DEFAULT_ACCEPTED_FILE_EXTENSIONS = { "csv" };
    
    private static String[] ACCEPTED_FILE_EXTENSIONS = DEFAULT_ACCEPTED_FILE_EXTENSIONS;
    
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String APPLICATION_OCTECT_STREAM = "application/octet-stream";
    private static final String CSV_FILE_EXTENSION = ".csv";
    private static final String DOT = ".";
    private static final String USER_AGENT = "User-Agent";

    private static final String ATTACHMENT_FILENAME = "attachment; filename=";

    private static final String OUTPUT_FILENAME_SUFFIX = "_out";
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
        String initParameter = config.getInitParameter(ACCEPTED_FILE_EXTENSIONS_KEY);
        ACCEPTED_FILE_EXTENSIONS = StringUtils.split(initParameter, COMMA);
    }   

    protected void perform(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        try {
            
            InputForm inputForm = new InputForm();
    
            inputForm.put(InputForm.AUS_GEOID, request.getParameter(InputForm.AUS_GEOID));
            inputForm.put(InputForm.DEGREE_TYPE, request.getParameter(InputForm.DEGREE_TYPE));

            inputForm.put(InputForm.HEIGHT_TYPE, request.getParameter(InputForm.HEIGHT_TYPE));

            FileItem fileItem = (FileItem) request.getAttribute("inputFile");

            String inputFileName = fileItem.getName();
            
            if (StringUtils.isBlank(inputFileName)) {
                throw new GeoidInterpolationException("No file name is entered!");
            }
            
            String extension = FilenameUtils.getExtension(inputFileName);
            boolean isAccepted = ArrayUtils.contains(ACCEPTED_FILE_EXTENSIONS, extension);
        
            if (!isAccepted) {
                throw new GeoidInterpolationException("Acceptable file extensions are " + Arrays.toString(ACCEPTED_FILE_EXTENSIONS));
            }

            long size = fileItem.getSize();
            if (size == 0) {
                throw new GeoidInterpolationException("File content is empty");
            }

            InputStream input = fileItem.getInputStream();
            
            Interpolant interpolant = buildInput(inputForm);

            InputStream output = this.geoidAdapter.interpolate(interpolant, input);

            buildResponse(request, response, output, inputFileName);

        } catch (GeoidInterpolationException ex) {

            handleError(request, response, ex);

        } catch (Throwable thr) {

            handleFatal(request, response, thr);

        }

    }

    private String getOutputAttachmentFilename(String filename) {

        StringBuilder sb = new StringBuilder();

        String outputFileName = StringUtils.substringBeforeLast(filename, DOT);

        sb.append(ATTACHMENT_FILENAME);
        sb.append(outputFileName);
        sb.append(OUTPUT_FILENAME_SUFFIX + CSV_FILE_EXTENSION);

        return sb.toString();
    }

    private void buildResponse(HttpServletRequest request,
            HttpServletResponse response, InputStream output,
            String inputFileName) {

        String userAgent = request.getHeader(USER_AGENT);

        /*
         * MS Internet Explorer 6.0: Mozilla/4.0 (compatible; MSIE 6.0; Windows
         * NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR
         * 3.5.30729)
         * 
         * Firefox: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.6)
         * Gecko/20091201 Firefox/3.5.6
         */

        if (!StringUtils.containsIgnoreCase(userAgent, "MSIE 6.0")) {
            response.setHeader(CONTENT_DISPOSITION,
                    getOutputAttachmentFilename(inputFileName));
        }

        response.setContentType(APPLICATION_OCTECT_STREAM);

        ServletOutputStream outputStream = null;

        try {
            outputStream = response.getOutputStream();
            byte[] outputByteArray = IOUtils.toByteArray(output);
            outputStream.write(outputByteArray);
            outputStream.flush();

        } catch (Exception e) {

            throw new GeoidInterpolationException(e.getMessage(), e);

        } finally {

            IOUtils.closeQuietly(outputStream);

        }
    }

}
