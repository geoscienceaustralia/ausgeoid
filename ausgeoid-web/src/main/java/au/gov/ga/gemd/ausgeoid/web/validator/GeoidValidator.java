package au.gov.ga.gemd.ausgeoid.web.validator;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;

import au.gov.ga.gemd.ausgeoid.web.GeoidValidationException;
import au.gov.ga.gemd.ausgeoid.web.form.InputForm;

public class GeoidValidator {

    private static final String FIELD_REQUIRED_KEY = "field.required";
    private static final String FIELD_NUMERIC_KEY = "field.numeric";
    private static final String FIELD_RANGE_KEY = "field.range";

    private static final double MAX_DEGREE = 180.00;
    private static final double MIN_DEGREE = -180.00;

    private static final ResourceBundle bundle = ResourceBundle.getBundle("messages");

    public void validate(InputForm inputForm) {
        
        String latitude = inputForm.get(InputForm.LATITUDE);
        String longitude = inputForm.get(InputForm.LONGITUDE);
        String height = inputForm.get(InputForm.HEIGHT);

        boolean isValid = true;

        List<String> messages = new ArrayList<String>();

        isValid = isValidLatLong(latitude, messages, InputForm.LATITUDE);
        isValid &= isValidLatLong(longitude, messages, InputForm.LONGITUDE);
        isValid &= isValidNumeric(height, messages, InputForm.HEIGHT);
        
        if (!isValid) {
            throw new GeoidValidationException(messages);
        }
    }

    private boolean isValidNumeric(String value, List<String> messages,
            String fieldName) {

        boolean isValid = true;

        if (StringUtils.isBlank(value)) {
            String message = MessageFormat.format(
                    bundle.getString(FIELD_REQUIRED_KEY),
                    new Object[] { fieldName });
            messages.add(message);
            isValid = false;
        } else {

            try {
                Double.parseDouble(value);
            } catch (NumberFormatException e) {
                String message = MessageFormat.format(
                        bundle.getString(FIELD_NUMERIC_KEY),
                        new Object[] { fieldName });
                messages.add(message);
                isValid = false;
            }
        }

        return isValid;
    }

    private boolean isValidLatLong(String latLong, List<String> messages,
            String fieldName) {

        boolean isValid = true;

        isValid = isValidNumeric(latLong, messages, fieldName);

        if (isValid) {
            double value = Double.parseDouble(latLong);
            if (value > MAX_DEGREE || value < MIN_DEGREE) {
                String message = MessageFormat.format(
                        bundle.getString(FIELD_RANGE_KEY), new Object[] {
                                fieldName, MIN_DEGREE, MAX_DEGREE });
                messages.add(message);
                isValid = false;
            }
        }

        return isValid;
    }
}
