package au.gov.ga.gemd.ausgeoid.web;

import java.util.List;

import au.gov.ga.gemd.geoid.core.GeoidInterpolationException;

@SuppressWarnings("serial")
public class GeoidValidationException extends GeoidInterpolationException {

    private List<String> messages;
    
    public GeoidValidationException(List<String> messages) {
        this.messages = messages;
    }

    public List<String> getMessages() {
        return this.messages;
    }

}
